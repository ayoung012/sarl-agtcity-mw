/** 
 * SARL-MASSIM - Interface between the SARL agent-oriented language 
 * and the MASSIM 2017 server
 * Copyright (C) 2017 The SARL-MASSIM Authors.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package au.edu.rmit.agtgrp.agtcity.sarl.mw.aggregator

import java.util.HashMap
import java.util.Collection
import eis.iilang.Percept

/** 
 * @author bob
 * 
 */
 
 /**
  * ItemVolumeBuffer, Class used to store the relationship between items and their volume.
  * @param volumeTable a HashMap containing mapping of String(Item name) to volume (Double). 
  */
class ItemVolumeBuffer {
	
	static var volumeTable = new HashMap<String, Double>
	
	
	/**
	 * populate, Function used to populate the volume Table and updates any changes in values
	 * @param percepts a collection of percepts to populate and update from.
	 */
	static def populate(percepts : Collection<Percept>){
		
		for(p : percepts){
			switch(p.name){
				case "item" : {
					var name = Util.extractString(p.parameters.get(0))
					var volume = Util.extractDouble(p.parameters.get(1))
					volumeTable.put(name, volume)
				}				
			}
		}
	}
	
	/**
	 * getVolume, lookup function used to find the volume of a particular item.
	 */
	static def getVolume(itemName : String) : double{
		var volume = volumeTable.get(itemName)
		if(volume === null){
			volume = 0.0d
		}
		return volume
	}
}
